---
title: 'Publication de la mise à jour de Debian 10.8'
date: 2020-01-02
---
Le projet Debian a l'honneur d'annoncer la septième mise à jour de sa distribution stable Debian 10 (nom de code Buster). Tout en réglant quelques problèmes importants, cette mise à jour corrige principalement des problèmes de sécurité de la version stable. Les annonces de sécurité ont déjà été publiées séparément et sont simplement référencées dans ce document.

Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de Debian 10 mais seulement une mise à jour de certains des paquets qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de la version Buster. Après installation, les paquets peuvent être mis à niveau vers les versions courantes en utilisant un miroir Debian à jour.

Ceux qui installent fréquemment les mises à jour à partir de security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour de security.debian.org sont comprises dans cette mise à jour.

De nouvelles images d'installation seront prochainement disponibles à leurs emplacements habituels.

Mettre à jour une installation vers cette révision peut se faire en faisant pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de Debian. Une liste complète des miroirs est disponible à l'adresse :

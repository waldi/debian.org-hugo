---
id: dsa-4843
---
Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

* CVE-2020-27815

  A flaw was reported in the JFS filesystem code allowing a local
  attacker with the ability to set extended attributes to cause a
  denial of service.

* CVE-2020-27825

  Adam <q>pi3</q> Zabrocki reported a use-after-free flaw in the ftrace
  ring buffer resizing logic due to a race condition, which could
  result in denial of service or information leak.

* CVE-2020-27830

  Shisong Qin reported a NULL pointer dereference flaw in the Speakup
  screen reader core driver.

---
title: 'Updated Debian 10: 10.8 released'
date: 2020-01-02
---
The Debian project is pleased to announce the seventh update of its stable distribution Debian 10 (codename buster). This point release mainly adds corrections for security issues, along with a few adjustments for serious problems. Security advisories have already been published separately and are referenced where available.

Please note that the point release does not constitute a new version of Debian 10 but only updates some of the packages included. There is no need to throw away old buster media. After installation, packages can be upgraded to the current versions using an up-to-date Debian mirror.

Those who frequently install updates from security.debian.org won't have to update many packages, and most such updates are included in the point release.

New installation images will be available soon at the regular locations.

Upgrading an existing installation to this revision can be achieved by pointing the package management system at one of Debian's many HTTP mirrors. A comprehensive list of mirrors is available at:
